defmodule JsonVideoConsumer.Repo do
  use Ecto.Repo,
    otp_app: :json_video_consumer,
    adapter: Ecto.Adapters.Postgres
end
