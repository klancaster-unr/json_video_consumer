defmodule JsonVideoConsumerWeb.Layouts do
  use JsonVideoConsumerWeb, :html

  embed_templates "layouts/*"
end
