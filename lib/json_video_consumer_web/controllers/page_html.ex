defmodule JsonVideoConsumerWeb.PageHTML do
  use JsonVideoConsumerWeb, :html

  embed_templates "page_html/*"
end
