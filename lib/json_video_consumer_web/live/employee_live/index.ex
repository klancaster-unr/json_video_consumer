defmodule JsonVideoConsumerWeb.EmployeeLive.Index do
  use JsonVideoConsumerWeb, :live_view

  alias JsonVideoConsumer.HumanResources

  @impl true
  def mount(_params, _session, socket) do
    {:ok, stream(socket, :employees, HumanResources.list_employees())}
  end



end
